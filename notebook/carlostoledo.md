# Carlos Toledo's Lab Notebook
### Each complete entry should include:
 - Date
 - Brief statement of objectives for that session
 - Record of what was done
## First Meeting After RFA Approval - 09/10/2023

### Objectives
 - initial design ideas
   - I2C v. Wireless
 - master / slave implementation ideas
   - addressing possibilities 

I made an initial idea for the design of our senior design project. Based on our approved RFA, our base system will have 2 moisture sensors, 1 solenoid valve, and at least 1 microcontroller. The system should me modular and allow dixie chaining to be a method of expansion. It should also attach to a standard garden hose. Here was my proposed design to the group. 

![Image](./images/Design.1.png)

We had concluded in our meeting today that a possible approach could involve using one microcontroller and creating a master slave system by assigning addresses to each pcb for each plant and communicate with each, cycling through an array of slave boards. We still have to figure out if this approach via I2C or adding addtional wireless capability will qualify as a 'dixie chained' system. This wil require an address system with some basic logic.


We will be discussing this possibility with our TA soon.

## Entry 2: 09/17/2023
### Objectives


## Entry 3: 09/24/2023
### Objectives


## Entry 4: 10/01/2023
### Objectives



## Entry 5: 10/08/2023
### Objectives



## Entry 6: 10/15/2023
### Objectives



## Entry 6: 10/22/2023
### Objectives



## Entry 7: 10/29/2023
### Objectives



## Entry 8: 11/05/2023
### Objectives



## Entry 9: 11/12/2023
### Objectives



## Entry 10: 11/19/2023
### Objectives


## Entry 11: 11/26/2023
### Objectives
